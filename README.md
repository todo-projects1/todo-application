# Todo Project Description

This repository contains the files for the on-boarding projects/exercises required by Pixel8 for Front-End Developers.

# Contains

- [ ] [Vue-Todo-Application](https://gitlab.com/todo-projects1/todo-application/-/tree/main/vue-todo-application/todo-project?ref_type=heads)

- [ ] [Quasar-Todo-Application](https://gitlab.com/todo-projects1/todo-application/-/tree/main/quasar-todo-application/todo-application?ref_type=heads)

## Author
Ruiz, Renzeus Marrius B.

## License
For open source projects, say how it is licensed.

## Project status
Onboarding execises are still on-going.
